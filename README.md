[TOC]

# Installation
Please click [here](https://gitlab.com/sayiarin/sayiavatartools/-/releases) to get to the latest release of the SayiAvatarTools.

To install them just download the zip file and just move its content into the unity project directory. This allows you to place the tools in whatever folder you wish without interfering with your asset storage conventions.

Alternatively you can download an installer [**here**](https://gitlab.com/sayiarin/sayiavatartools/-/wikis/uploads/031b947522f81e6fad2fd363277b80be/SayiAvatarToolsInstaller.unitypackage).
After you have added the unity package to your project follow the instructions in the Update section to get the latest version.

# Update
To update, please select `Tools -> SayiTools -> Update` in your Unity Editor. This will open a new window which checks if a newer version is available and can install the most up to date version automatically by the press of a button.

# Shaders
For a list of shaders as well as explanation and preview images have a look [here](Shader/README.md).

# Tools
## Texture Arrays
### TextureArrayManagerEditor
This allows for a much nicer workflow when working with Texture2DArrays because this object will keep references to the original images and the array size and content can be changed at will.

If no Texture2DArray exists yet this tool will create one. Existing ones are referenced here as well and will be properly overwriten while keeping references on other objects intact.

To create an Asset of this just `Rightclick -> Create -> SayiTools -> TextureArrayManager`.

### Create Texture Array From Selection
Right click any amount of textures in your Asset Database and `Rightclick -> SayiTools -> Create Texture Array From Selection`. This will create a new Texture Array Asset in the same place as the selected textures containing every image in the selection.

## Noise Generator
Opens a new Dialog window with some settings to generate a simple Perlin Noise texture. There are options for Noise Level and Resolution.

Available either through `Tools -> SayiTools -> NoiseGenerator` or `Rightclick -> SayiTools -> Generate Perlin Noise Texture`.
