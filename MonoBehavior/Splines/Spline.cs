﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace SayiTools
{
    [DisallowMultipleComponent]
    [AddComponentMenu("SayiTools/Spline")]
    public class Spline : MonoBehaviour
    {
        private List<Vector3> points;
        private List<ControlPointMode> modes;
        private bool loop;

        // selection state
        public int selectedIndex = -1;
        public int selectedAnchor = -1;
        public Transform handleTransform;
        public Quaternion handleRotation;

        // spline decorator
        public bool enableDecorator = false;
        public List<GameObject> decoratorModels;
        public List<GameObject> spawnedObjects = new List<GameObject>();
        public int decoratorFrequency = 1;
        public bool decoratorFacesDirection = false;
        public Vector3 decoratorRotation = Vector3.zero;
        public Vector3 decoratorScale = Vector3.one;

        // mesh generation
        public bool enableMeshGeneration = false;
        public List<Vector3> vertices = new List<Vector3>();
        public List<Vector2> uv = new List<Vector2>();
        public List<int> triangles = new List<int>();

        public bool Loop
        {
            get
            {
                return loop;
            }
            set
            {
                loop = value;
                if (value)
                {
                    modes[modes.Count - 1] = modes[0];
                    SetControlPoint(0, points[0]);
                }
            }
        }

        public int ControlPointCount
        {
            get
            {
                return points.Count;
            }
        }

        public Vector3 GetControlPoint(int index)
        {
            return points[index];
        }

        public void SetControlPoint(int index, Vector3 point)
        {
            if (index % 3 == 0)
            {
                Vector3 delta = point - points[index];
                if (loop)
                {
                    if (index == 0)
                    {
                        points[1] += delta;
                        points[points.Count - 2] += delta;
                        points[points.Count - 1] = point;
                    }
                    else if (index == points.Count - 1)
                    {
                        points[0] = point;
                        points[1] += delta;
                        points[index - 1] += delta;
                    }
                    else
                    {
                        points[index - 1] += delta;
                        points[index + 1] += delta;
                    }
                }
                else
                {
                    if (index > 0)
                    {
                        points[index - 1] += delta;
                    }
                    if (index + 1 < points.Count)
                    {
                        points[index + 1] += delta;
                    }
                }
            }
            points[index] = point;
            EnforceMode(index);
        }

        public ControlPointMode GetControlPointMode(int index)
        {
            return modes[(index + 1) / 3];
        }

        public void SetControlPointMode(int index, ControlPointMode mode)
        {
            int modeIndex = (index + 1) / 3;
            modes[modeIndex] = mode;
            if (loop)
            {
                if (modeIndex == 0)
                {
                    modes[modes.Count - 1] = mode;
                }
                else if (modeIndex == modes.Count - 1)
                {
                    modes[0] = mode;
                }
            }
            EnforceMode(index);
        }

        private void EnforceMode(int index)
        {
            int modeIndex = (index + 1) / 3;
            ControlPointMode mode = modes[modeIndex];
            if (mode == ControlPointMode.Free || !loop && (modeIndex == 0 || modeIndex == modes.Count - 1))
            {
                return;
            }

            int middleIndex = modeIndex * 3;
            int fixedIndex, enforcedIndex;
            if (index <= middleIndex)
            {
                fixedIndex = middleIndex - 1;
                if (fixedIndex < 0)
                {
                    fixedIndex = points.Count - 2;
                }
                enforcedIndex = middleIndex + 1;
                if (enforcedIndex >= points.Count)
                {
                    enforcedIndex = 1;
                }
            }
            else
            {
                fixedIndex = middleIndex + 1;
                if (fixedIndex >= points.Count)
                {
                    fixedIndex = 1;
                }
                enforcedIndex = middleIndex - 1;
                if (enforcedIndex < 0)
                {
                    enforcedIndex = points.Count - 2;
                }
            }

            Vector3 middle = points[middleIndex];
            Vector3 enforcedTangent = middle - points[fixedIndex];
            if (mode == ControlPointMode.Aligned)
            {
                enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
            }
            points[enforcedIndex] = middle + enforcedTangent;
        }

        public int CurveCount
        {
            get
            {
                return (points.Count - 1) / 3;
            }
        }

        public Vector3 GetPoint(float t)
        {
            int i;
            if (t >= 1f)
            {
                t = 1f;
                i = points.Count - 4;
            }
            else
            {
                t = Mathf.Clamp01(t) * CurveCount;
                i = (int)t;
                t -= i;
                i *= 3;
            }
            return transform.TransformPoint(Bezier.GetPoint(points[i], points[i + 1], points[i + 2], points[i + 3], t));
        }

        public Vector3 GetVelocity(float t)
        {
            int i;
            if (t >= 1f)
            {
                t = 1f;
                i = points.Count - 4;
            }
            else
            {
                t = Mathf.Clamp01(t) * CurveCount;
                i = (int)t;
                t -= i;
                i *= 3;
            }
            return transform.TransformPoint(Bezier.GetFirstDerivative(points[i], points[i + 1], points[i + 2], points[i + 3], t)) - transform.position;
        }

        public Vector3 GetDirection(float t)
        {
            return GetVelocity(t).normalized;
        }

        public void AddCurve()
        {
            Vector3 point = points[points.Count - 1];
            point.x += 1f;
            points.Add(point);
            point.x += 1f;
            points.Add(point);
            point.x += 1f;
            points.Add(point);

            modes.Add(modes[modes.Count - 1]);
            EnforceMode(points.Count - 4);

            if (loop)
            {
                points[points.Count - 1] = points[0];
                modes[modes.Count - 1] = modes[0];
                EnforceMode(0);
            }
        }

        public void DeleteCurve(int anchorIdx)
        {
            if (anchorIdx % 3 != 0)
            {
                return;
            }

            int lastPoint = points.Count - 1;

            if (anchorIdx == 0)
            {
                points.RemoveAt(0);
                points.RemoveAt(0);
                points.RemoveAt(0);
            }
            else if (anchorIdx == lastPoint)
            {
                points.RemoveAt(anchorIdx - 0);
                points.RemoveAt(anchorIdx - 1);
                points.RemoveAt(anchorIdx - 2);
            }
            else
            {
                points.RemoveAt(anchorIdx);
                points.RemoveAt(anchorIdx);
                points.RemoveAt(anchorIdx - 1);
            }

            int modeidx = (anchorIdx + 1) / 3;
            modes.RemoveAt(modeidx);
        }

        public void Reset()
        {
            points = new List<Vector3>
            {
                new Vector3(1f, 0f, 0f),
                new Vector3(2f, 0f, 0f),
                new Vector3(3f, 0f, 0f),
                new Vector3(4f, 0f, 0f)
            };
            modes = new List<ControlPointMode>
            {
                ControlPointMode.Mirrored,
                ControlPointMode.Mirrored
            };
        }

        public void AlignHandlesOnAxis(int idx, SnapAxis axis)
        {
            if (idx % 3 != 0)
            {
                // not an anchor, nothing to do
                return;
            }

            Vector3 anchor = points[idx];
            if (idx != 0)
            {
                AlignHandleWithAnchor(idx - 1, anchor, axis);
            }
            if (idx != points.Count - 1)
            {
                AlignHandleWithAnchor(idx + 1, anchor, axis);
            }
        }

        private void AlignHandleWithAnchor(int handleIdx, Vector3 anchor, SnapAxis axis)
        {
            Vector3 handle = points[handleIdx];
            switch (axis)
            {
                case SnapAxis.X:
                    handle.x = anchor.x;
                    break;
                case SnapAxis.Y:
                    handle.y = anchor.y;
                    break;
                case SnapAxis.Z:
                    handle.z = anchor.z;
                    break;

                default:
                    throw new NotSupportedException("operation for axis setting " + axis + "not implemented");
            }
            points[handleIdx] = handle;
        }

        public void AlignAllAnchorsOnAxis(SnapAxis axis)
        {
            Vector3 anchor;
            Vector3 handleA;
            Vector3 handleB = Vector3.zero;

            for (int i = 1; i < points.Count; i++)
            {
                // only anchors
                if (i % 3 == 0)
                {
                    anchor = points[i];

                    handleA = anchor - points[i - 1];
                    if (i < points.Count - 1)
                    {
                        handleB = anchor - points[i + 1];
                    }

                    if (axis == SnapAxis.X)
                    {
                        anchor.x = points[0].x;
                    }
                    else if (axis == SnapAxis.Y)
                    {
                        anchor.y = points[0].y;
                    }
                    else
                    {
                        anchor.z = points[0].z;
                    }
                    points[i] = anchor;

                    points[i - 1] = anchor - handleA;

                    if (i < points.Count - 1)
                    {
                        points[i + 1] = anchor - handleB;
                    }
                }
            }
        }

        public void Decorate()
        {
            ClearSpawnedObjects();

            for (int f = 0; f < decoratorFrequency; f++)
            {
                for (int i = 0; i < decoratorModels.Count; i++)
                {
                    if (decoratorModels[i] == null)
                    {
                        continue;
                    }

                    GameObject proxy = new GameObject(decoratorModels[i].name);
                    spawnedObjects.Add(proxy);
                    proxy.transform.parent = transform;

                    GameObject instance = Instantiate(decoratorModels[i]);
                    instance.transform.parent = proxy.transform;
                    instance.transform.localPosition = Vector3.zero;
                }
            }

            ApplyAllDecoratorTransforms();

        }

        // not very optimised, but it's okay for the current scope
        public void ApplyAllDecoratorTransforms()
        {
            ApplyDecoratorTranslation();
            ApplyDecoratorScale();
            ApplyDecoratorRotation();
        }

        public void ApplyDecoratorTranslation()
        {
            float offset = spawnedObjects.Count == 1 ? .5f : 1f / (spawnedObjects.Count - 1);
            for (int i = 0; i < spawnedObjects.Count; i++)
            {
                Vector3 position = GetPoint(i * offset);
                spawnedObjects[i].transform.position = position;
            }
        }

        public void ApplyDecoratorScale()
        {
            foreach (GameObject obj in spawnedObjects)
            {
                obj.transform.localScale = decoratorScale;
            }
        }

        public void ApplyDecoratorRotation()
        {
            float offset = spawnedObjects.Count == 1 ? .5f : 1f / (spawnedObjects.Count - 1);
            GameObject obj;
            Vector3 splineDirection;
            for (int i = 0; i < spawnedObjects.Count; i++)
            {
                obj = spawnedObjects[i];
                if (decoratorFacesDirection)
                {
                    splineDirection = GetDirection(i * offset);
                    obj.transform.LookAt(obj.transform.position + splineDirection);
                }
                else
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                if (obj.transform.childCount > 0)
                {
                    obj.transform.GetChild(0).rotation = Quaternion.Euler(decoratorRotation) * obj.transform.rotation;
                }
            }
        }

        public void ClearSpawnedObjects()
        {
            foreach (GameObject obj in spawnedObjects)
            {
                DestroyImmediate(obj);
            }
            spawnedObjects.Clear();
        }
    }
}