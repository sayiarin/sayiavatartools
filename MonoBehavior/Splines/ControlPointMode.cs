﻿namespace SayiTools
{
	public enum ControlPointMode
	{
		Free,
		Aligned,
		Mirrored
	}
}