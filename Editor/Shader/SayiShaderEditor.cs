﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;

public abstract class SayiShaderEditor : ShaderGUI
{
    protected MaterialProperty[] MatProperties;
    protected MaterialEditor MatEditor;
    protected Material Mat;

    public abstract void DrawGUI();

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        MatEditor = materialEditor;
        MatProperties = properties;
        Mat = materialEditor.target as Material;

        DrawGUI();
    }

    // simple wrapper for function in EditorGUIHelper for my shaders
    protected void DrawFoldoutWithOptions(string title, string toggleKey, Action optionFunc)
    {
        DrawFoldoutWithOptions(title, "", toggleKey, optionFunc);
    }

    // simple wrapper for function in EditorGUIHelper for my shaders
    public void DrawFoldoutWithOptions(string title, string infoMessage, string toggleKey, Action optionFunc)
    {
        SayiTools.EditorGUIHelper.DrawFoldout(title, infoMessage, toggleKey, optionFunc, Mat, true);
    }

    protected float FloatProperty(string propertyName, string label)
    {
        MaterialProperty prop = FindProperty(propertyName, MatProperties);
        return MatEditor.FloatProperty(prop, label);
    }

    protected void RangeProperty(string propertyName, string label)
    {
        MaterialProperty prop = FindProperty(propertyName, MatProperties);
        MatEditor.RangeProperty(prop, label);
    }

    protected Texture TextureProperty(string propertyName, string label)
    {
        MaterialProperty prop = FindProperty(propertyName, MatProperties);
        return MatEditor.TextureProperty(prop, label);
    }

    protected void ColourProperty(string propertyName, string label)
    {
        MaterialProperty prop = FindProperty(propertyName, MatProperties);
        MatEditor.ColorProperty(prop, label);
    }

    protected bool ToggleProperty(string propertyName, string label)
    {
        MaterialProperty prop = FindProperty(propertyName, MatProperties);
        bool state = prop.floatValue != 0.0f;

        state = EditorGUILayout.Toggle(label, state);
        prop.floatValue = state ? 1.0f : 0.0f;

        return state;
    }

    protected bool ToggleKeyword(string keyword, string label)
    {
        bool state = Mat.IsKeywordEnabled(keyword);
        state = EditorGUILayout.Toggle(label, state);
        if (state)
        {
            Mat.EnableKeyword(keyword);
        }
        else
        {
            Mat.DisableKeyword(keyword);
        }

        return state;
    }
}
#endif