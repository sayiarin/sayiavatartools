﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;

public class SayiPixelationShaderEditor : SayiShaderEditor
{
    private const string DistanceInfoMessage = "Affected by distance (enabled by default) means that the \"Pixels\" get smaller the further away an object is.\nContrary, when this setting is disabled the \"Pixels\" will stay the same size on the screen regardless of the distance to the pixelated object to the camera.";

    private float unifiedPixelSize = 0.05f;
    private bool setSizePerAxis = false;

    public override void DrawGUI()
    {
        SayiTools.EditorGUIHelper.HeaderLevel1("Pixelation Shader");

        EditorGUI.BeginDisabledGroup(setSizePerAxis);
        unifiedPixelSize = EditorGUILayout.Slider("Pixel Size", unifiedPixelSize, 0.0f, 0.2f);
        ApplyUnifiedPixelSize();
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space();

        setSizePerAxis = EditorGUILayout.Toggle("Change Pixel size per Axis", setSizePerAxis);
        EditorGUI.BeginDisabledGroup(!setSizePerAxis);
        RangeProperty("_PixelSizeX", "Pixel Size X");
        RangeProperty("_PixelSizeY", "Pixel Size Y");
        EditorGUI.EndDisabledGroup();

        SayiTools.EditorGUIHelper.Separator();

        ToggleProperty("_AffectedByDistance", "Affected by Distance");
        EditorGUILayout.HelpBox(DistanceInfoMessage, MessageType.Info);
    }

    private void ApplyUnifiedPixelSize()
    {
        if (!setSizePerAxis)
        {
            Mat.SetFloat("_PixelSizeX", unifiedPixelSize);
            Mat.SetFloat("_PixelSizeY", unifiedPixelSize);
        }
    }
}
#endif