﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

// sadly the Editor GUI only works if is in the default namespace :<
public class SayiToonShaderEditor : SayiShaderEditor
{
    private enum RenderType
    {
        Opaque, Transparent, Cutout
    }

    private const string FallbackInfo = "These settings are for the Unity Standard Shader to control how your avatar looks to people that don't have your shader shown.";
    private const string MaterialSettingsInfoText = @"The Material Feature mask will maps features to parts of your texture using the different colour channels to distinguish between them.
For ease of use make sure you use the Texture Combiner(Tools > Sayi > ImageCombiner) to create the final mask, using black white as the basis for each colour channel will yield the best result.
The setup is as follows:
R - reflection/smoothness
G - specular highlights
B - world position texture";
    private const string SpecialEffectsInfoText = @"The Special Feature Mask maps special effects to parts of your texture using the different colour channels to distinguish between them.
For ease of use make sure you use the Texture Combiner(Tools > Sayi > ImageCombiner) to create the final mask, using black white as the basis for each colour channel will yield the best result.
The setup is as follows:
R - Hue/Saturation/Value Changes
G - Wireframe
B - Rainbow Effect
A - Colour Inversion";
    private const string GlowInfoMessage = "For the glow provide a texture. The glow will appear in the colour of any part, transparent values will mean the base texture is rendered instead.";

    private RenderType renderType;
    private bool isSimpleVariant;

    public override void DrawGUI()
    {
        SayiTools.EditorGUIHelper.HeaderLevel1("Sayi Toon");

        DrawFoldoutWithOptions("General Settings", "ShowGeneralSettings", GeneralSettings);
        DrawFoldoutWithOptions("Shadows", "ShowShadowSettings", ShadowSettings);
        DrawFoldoutWithOptions("MatCap", "ShowMatCapSettings", MatCapSettings);
        DrawFoldoutWithOptions("Material Settings", "ShowMaterialSettings", MaterialSettings);
        DrawFoldoutWithOptions("Special Effects", "ShowSpecialEffectsSettings", SpecialEffects);
        DrawFoldoutWithOptions("Fallback Settings", FallbackInfo, "ShowFallbackSettings", FallbackSettings);
    }

    private void FallbackSettings()
    {
        // Base Tex is using the _MainTex property so don't show it here if simple variant is used
        if (!isSimpleVariant)
        {
            TextureProperty("_MainTex", "Fallback Texture");
        }
        RangeProperty("_Glossiness", "Smoothness");
        RangeProperty("_Metallic", "Metallic");
    }

    private void GeneralSettings()
    {
        MaterialProperty cullMode = FindProperty("_CullMode", MatProperties);
        MatEditor.ShaderProperty(cullMode, "Cull Mode");

        ShowRenderTypeSettings();

        MatEditor.RenderQueueField();
        bool litState = ToggleKeyword("SAYI_LIT", "Affected by Environment Lighting");
        Mat.SetShaderPassEnabled("SayiToonForwardAdd", litState);
        ToggleProperty("_ApplyFog", "Affected by Fog");

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);


        isSimpleVariant = ToggleKeyword("SAYI_SIMPLE", "Use single Texture instead of Tex2DArray");
        if (isSimpleVariant)
        {
            TextureProperty("_MainTex", "Base Texture");
        }
        else
        {
            Texture2DArrayProperty("_BaseTextures", "Texture Array", "_BaseTextureIndex", "Texture Index", "_LastBaseTexIdx");
        }

        RangeProperty("_Contrast", "Contrast");
        ColourProperty("_BaseTexTint", "Base Texture Tint");

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);
        TextureProperty("_NormalMap", "Normal Map");
        RangeProperty("_NormalMapStrength", "Normal Map Strength");
        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);
    }

    private void ShowRenderTypeSettings()
    {
        EditorGUI.BeginChangeCheck();

        switch (Mat.GetTag("RenderType", false).ToLower())
        {
            case "opaque":
                renderType = RenderType.Opaque;
                break;

            case "transparent":
                renderType = RenderType.Transparent;
                break;

            case "transparentcutout":
                renderType = RenderType.Cutout;
                break;

            default:
                throw new System.Exception("Unable to determine RenderType for SayiToon Shader");
        }

        renderType = (RenderType)EditorGUILayout.EnumPopup("Render Type", renderType);

        if (EditorGUI.EndChangeCheck())
        {
            switch (renderType)
            {
                case RenderType.Opaque:
                    Mat.SetOverrideTag("RenderType", "Opaque");
                    Mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    Mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    Mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Geometry;
                    Mat.DisableKeyword("SAYI_TRANSPARENT");
                    Mat.DisableKeyword("SAYI_CUTOUT");
                    break;

                case RenderType.Transparent:
                    Mat.SetOverrideTag("RenderType", "Transparent");
                    Mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    Mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    Mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
                    Mat.EnableKeyword("SAYI_TRANSPARENT");
                    Mat.DisableKeyword("SAYI_CUTOUT");
                    break;

                case RenderType.Cutout:
                    Mat.SetOverrideTag("RenderType", "TransparentCutout");
                    Mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    Mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    Mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.AlphaTest;
                    Mat.EnableKeyword("SAYI_CUTOUT");
                    Mat.DisableKeyword("SAYI_TRANSPARENT");
                    break;
            }
        }
        if (renderType == RenderType.Cutout)
        {
            RangeProperty("_AlphaCutoff", "Alpha Cutoff");
        }
    }

    private void ShadowSettings()
    {
        bool shadowEnabled = ToggleProperty("_EnableDirectionalShadow", "Enable Directional Shadows");
        EditorGUI.BeginDisabledGroup(!shadowEnabled);
        RangeProperty("_ShadowStrength", "Shadow Strength");
        RangeProperty("_ShadowSmoothness", "Shadow Smoothness");

        bool shadowRampEnabled = ToggleProperty("_EnableShadowRamp", "Enable Shadow Ramp");
        EditorGUI.BeginDisabledGroup(!shadowRampEnabled);
        TextureProperty("_ShadowRamp", "Shadow Ramp Texture");
        EditorGUI.EndDisabledGroup();
        EditorGUI.EndDisabledGroup();
    }

    private void MaterialSettings()
    {
        EditorGUILayout.HelpBox(MaterialSettingsInfoText, MessageType.Info);

        if (isSimpleVariant)
        {
            TextureProperty("_MaterialFeatureMask", "Material Feature Mask");
        }
        else
        {
            Texture2DArrayProperty("_MaterialFeatureMaskArray", "Material Feature Masks", "_MaterialFeatureMaskIndex", "Texture Index", "_LastMaterialTexIdx");
        }

        RangeProperty("_Smoothness", "Smoothness");
        RangeProperty("_Reflectiveness", "Reflectiveness");
        ToggleProperty("_ReflectionIgnoresAlpha", "Allow Reflections on Transparent Surface");
        RangeProperty("_SpecularHighlightExponent", "Specular Highlight Exponent");
        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);
        DrawFoldoutWithOptions("WorldPos Texture", "ShowWorldPosTexSettings", WorldPosTexSettings);
    }

    private void MatCapSettings()
    {
        bool matCapEnabled = ToggleKeyword("SAYI_MATCAP", "Enable MatCap");
        EditorGUI.BeginDisabledGroup(!matCapEnabled);
        TextureProperty("_MatCapTexture", "Texture");
        ColourProperty("_MatCapColourBias", "Colour Bias");
        RangeProperty("_MatCapLightingOverride", "Lighting Override");
        RangeProperty("_MatCapReflectionOverride", "Reflection Override");
        EditorGUI.EndDisabledGroup();
    }

    private void WorldPosTexSettings()
    {
        bool worldPosTexEnabled = ToggleProperty("_EnableWorldPosTexture", "World Position Texture");
        EditorGUI.BeginDisabledGroup(!worldPosTexEnabled);
        TextureProperty("_WorldPosTexture", "Texture");
        RangeProperty("_WorldPosTextureZoom", "Zoom");
        EditorGUI.EndDisabledGroup();
    }

    private void SpecialEffects()
    {
        EditorGUILayout.HelpBox(SpecialEffectsInfoText, MessageType.Info);

        if (isSimpleVariant)
        {
            TextureProperty("_SpecialFeatureMask", "Special Feature Mask");
        }
        else
        {
            Texture2DArrayProperty("_SpecialFeatureMaskArray", "Special Effects Feature Masks", "_SpecialFeatureMaskIndex", "Texture Index", "_LastSpecialTexIdx");
        }

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);

        ColourProperty("_HSVTexTint", "HSV Tint");

        RangeProperty("_HueShift", "Hue");
        RangeProperty("_SaturationValue", "Saturation");
        RangeProperty("_ColourValue", "Value");

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);

        bool wireframeEnabled = ToggleProperty("_EnableWireframe", "Wireframe");
        EditorGUI.BeginDisabledGroup(!wireframeEnabled);
        RangeProperty("_WireframeWidth", "Width");
        ColourProperty("_WireframeColour", "Colour");
        RangeProperty("_WireframeFadeOutDistance", "Fadeout Distance");
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);

        bool rainbowEnabled = ToggleProperty("_EnableRainbowEffect", "Enable Rainbow Effect");
        EditorGUI.BeginDisabledGroup(!rainbowEnabled);
        RangeProperty("_RainbowSpeed", "Speed");
        RangeProperty("_RainbowWaveSize", "Wave Size");
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);

        ToggleProperty("_InvertColours", "Invert Colours");

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);

        bool outlineEnabled = ToggleProperty("_EnableOutline", "Enable Outline");
        EditorGUI.BeginDisabledGroup(!outlineEnabled);
        RangeProperty("_OutlineWidth", "Width");
        ColourProperty("_OutlineColour", "Colour");
        ToggleProperty("_OutlineFoggy", "Affected by Fog");
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space(EditorGUIUtility.singleLineHeight);

        DrawFoldoutWithOptions("Glow Effect", "ShowGlowEffect", GlowEffect);
    }

    private void GlowEffect()
    {
        bool glowEnabled = ToggleProperty("_EnableGlow", "Enable Glow Effect");
        EditorGUI.BeginDisabledGroup(!glowEnabled);
        if (glowEnabled)
        {
            EditorGUILayout.HelpBox(GlowInfoMessage, MessageType.Info);
        }
        TextureProperty("_GlowTexture", "Glow Texture");
        RangeProperty("_GlowIntensity", "Glow Intensity");
        bool glowChange = ToggleProperty("_EnableGlowColourChange", "enable Glow Colour Change over Time");
        EditorGUI.BeginDisabledGroup(!glowChange);
        RangeProperty("_GlowSpeed", "Colour Change Speed");
        EditorGUI.EndDisabledGroup();
        EditorGUI.EndDisabledGroup();
    }

    // clamps values between length of tex2D array but also sets the last index property in my shader which allows it to loop back around from the last to the first texture
    private void Texture2DArrayProperty(string arrayName, string arrayLabel, string idxName, string idxLabel, string lastIdxName)
    {
        Texture2DArray texArray = (Texture2DArray)TextureProperty(arrayName, arrayLabel);
        float idx = FloatProperty(idxName, idxLabel);
        if(texArray == null)
        {
            Mat.SetFloat(lastIdxName, 0);
            return;
        }

        if (idx < 0)
        {
            Mat.SetFloat(idxName, texArray.depth - 0.01f);
        }
        else if (idx >= texArray.depth)
        {
            Mat.SetFloat(idxName, 0);
        }
        Mat.SetInt(lastIdxName, texArray.depth - 1);
    }
}
#endif