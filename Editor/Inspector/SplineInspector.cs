﻿using UnityEditor;
using UnityEngine;

namespace SayiTools
{
    [CustomEditor(typeof(Spline))]
    public class SplineInspector : Editor
    {
        private const int stepsPerCurve = 10;

        private static Color[] modeColors = {
            Color.white,
            Color.green,
            Color.cyan
        };

        private Spline spline;

        // visualisation
        private GUIStyle highlightedStyle;
        private float directionScale = 1f;
        private float handleSize = .05f;

        public override void OnInspectorGUI()
        {
            EditorGUIHelper.HeaderLevel2("Spline Settings");
            spline = target as Spline;
            EditorGUI.BeginChangeCheck();
            bool loop = EditorGUILayout.Toggle("Loop", spline.Loop);
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(spline);
                spline.Loop = loop;
            }

            EditorGUIHelper.DrawFoldout("Point Data", "Spline", DrawPointGUI, this);

            if (GUILayout.Button("Add Point"))
            {
                spline.AddCurve();
                EditorUtility.SetDirty(spline);
            }

            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Align All Anchors on X"))
            {
                spline.AlignAllAnchorsOnAxis(SnapAxis.X);
                EditorUtility.SetDirty(spline);
            }
            if (GUILayout.Button("Align All Anchors on Y"))
            {
                spline.AlignAllAnchorsOnAxis(SnapAxis.Y);
                EditorUtility.SetDirty(spline);
            }
            if (GUILayout.Button("Align All Anchors on Z"))
            {
                spline.AlignAllAnchorsOnAxis(SnapAxis.Z);
                EditorUtility.SetDirty(spline);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space(10);
            EditorGUIHelper.DrawFoldout("Decorator", "SplineDecorator", DrawDecoratorGUI, this);
            EditorGUILayout.Space(10);
            EditorGUIHelper.DrawFoldout("Mesh Generator", "SplineMeshGenerator", DrawMeshGenerationGUI, this);
            EditorGUILayout.Space(10);

            EditorGUIHelper.DrawFoldout("Visualisation Settings", "UISettings", DrawUISettings, this);
        }

        private void DrawPointGUI()
        {
            for (int i = 0; i < spline.ControlPointCount; i++)
            {
                // only anchors
                if (i % 3 == 0)
                {
                    if (i == spline.selectedAnchor)
                    {
                        if (highlightedStyle == null)
                        {
                            highlightedStyle = new GUIStyle();
                            highlightedStyle.normal.background = Texture2D.grayTexture;
                        }
                        EditorGUILayout.BeginVertical(highlightedStyle);
                    }
                    EditorGUILayout.BeginHorizontal();
                    DrawAnchorGUI(i);

                    EditorGUI.BeginDisabledGroup(spline.CurveCount < 2);
                    if (GUILayout.Button("X"))
                    {
                        spline.selectedIndex = 0;
                        spline.selectedAnchor = 0;
                        spline.DeleteCurve(i);
                        EditorUtility.SetDirty(spline);
                    }
                    EditorGUI.EndDisabledGroup();

                    EditorGUILayout.EndHorizontal();
                    if (i == spline.selectedAnchor)
                    {
                        EditorGUILayout.EndVertical();
                    }

                    EditorGUILayout.Space(10);
                }
            }
        }

        private void DrawAnchorGUI(int index)
        {
            EditorGUILayout.BeginVertical();
            EditorGUI.BeginChangeCheck();
            Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(index));
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(spline);
                spline.SetControlPoint(index, point);
            }
            EditorGUI.BeginChangeCheck();
            ControlPointMode mode = (ControlPointMode)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(index));
            if (EditorGUI.EndChangeCheck())
            {
                spline.SetControlPointMode(index, mode);
                EditorUtility.SetDirty(spline);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Align Handles on Axis");
            if (GUILayout.Button("X"))
            {
                spline.AlignHandlesOnAxis(index, SnapAxis.X);
                EditorUtility.SetDirty(spline);
            }
            if (GUILayout.Button("Y"))
            {
                spline.AlignHandlesOnAxis(index, SnapAxis.Y);
                EditorUtility.SetDirty(spline);
            }
            if (GUILayout.Button("Z"))
            {
                spline.AlignHandlesOnAxis(index, SnapAxis.Z);
                EditorUtility.SetDirty(spline);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void DrawDecoratorGUI()
        {
            spline.enableDecorator = EditorGUILayout.Toggle("Enable Decorator", spline.enableDecorator);
            if (spline.enableDecorator == false)
            {
                spline.ClearSpawnedObjects();
            }

            EditorGUI.BeginDisabledGroup(spline.enableDecorator == false);
            EditorGUI.BeginChangeCheck();
            spline.decoratorRotation = EditorGUILayout.Vector3Field("Rotation", spline.decoratorRotation);
            if (EditorGUI.EndChangeCheck())
            {
                spline.ApplyDecoratorRotation();
                EditorUtility.SetDirty(spline);
            }

            EditorGUI.BeginChangeCheck();
            spline.decoratorScale = EditorGUILayout.Vector3Field("Scale", spline.decoratorScale);
            if (EditorGUI.EndChangeCheck())
            {
                spline.ApplyDecoratorScale();
                EditorUtility.SetDirty(spline);
            }

            EditorGUI.BeginChangeCheck();
            spline.decoratorFrequency = EditorGUILayout.IntField("Frequency", spline.decoratorFrequency);
            spline.decoratorFrequency = Mathf.Clamp(spline.decoratorFrequency, 0, 512);
            if (EditorGUI.EndChangeCheck())
            {
                spline.Decorate();
                EditorUtility.SetDirty(spline);
            }

            EditorGUI.BeginChangeCheck();
            spline.decoratorFacesDirection = EditorGUILayout.Toggle("Objects Face Spline Direction", spline.decoratorFacesDirection);
            if (EditorGUI.EndChangeCheck())
            {
                spline.ApplyDecoratorRotation();
                EditorUtility.SetDirty(spline);
            }

            EditorGUILayout.Space(10);
            SerializedProperty decoraterModelsProp = serializedObject.FindProperty("decoratorModels");
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(decoraterModelsProp);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
                spline.Decorate();
                EditorUtility.SetDirty(spline);
            }

            if (GUILayout.Button("Finalise Objects"))
            {
                GameObject obj = new GameObject(spline.name);
                obj.transform.parent = spline.transform.parent;
                obj.transform.localPosition = spline.transform.localPosition;
                obj.transform.localRotation = spline.transform.localRotation;
                obj.transform.localScale = spline.transform.localScale;

                foreach (GameObject spawnedObject in spline.spawnedObjects)
                {
                    spawnedObject.transform.parent = obj.transform;
                }
                spline.spawnedObjects.Clear();
            }
            EditorGUI.EndDisabledGroup();
        }

        private void DrawMeshGenerationGUI()
        {
            
        }

        private void DrawUISettings()
        {
            EditorGUI.BeginChangeCheck();
            directionScale = EditorGUILayout.Slider("Direction Scale", directionScale, 0.1f, 3f);
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(spline);
            }

            EditorGUI.BeginChangeCheck();
            handleSize = EditorGUILayout.Slider("Handle Size", handleSize, .01f, .2f);
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(spline);
            }

            if (GUILayout.Button("Reset"))
            {
                directionScale = 1f;
                handleSize = .05f;
                EditorUtility.SetDirty(spline);
            }
        }

        private void OnSceneGUI()
        {
            spline = target as Spline;
            spline.handleTransform = spline.transform;
            spline.handleRotation = Tools.pivotRotation == PivotRotation.Local ?
                spline.handleTransform.rotation : Quaternion.identity;

            Vector3 p0 = ShowPoint(0, true);
            for (int i = 1; i < spline.ControlPointCount; i += 3)
            {
                Vector3 p1 = ShowPoint(i);
                Vector3 p2 = ShowPoint(i + 1);
                // last point is always anchor, we want to colour it differently
                Vector3 p3 = ShowPoint(i + 2, true);

                Handles.color = Color.gray;
                Handles.DrawLine(p0, p1);
                Handles.DrawLine(p2, p3);

                Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2f);
                p0 = p3;
            }
            ShowDirections();
        }

        private void ShowDirections()
        {
            Handles.color = Color.magenta;
            Vector3 point = spline.GetPoint(0f);

            Handles.DrawLine(point, point + spline.GetDirection(0f) * directionScale);
            int steps = stepsPerCurve * spline.CurveCount;
            for (int i = 1; i <= steps; i++)
            {
                point = spline.GetPoint(i / (float)steps);
                Handles.DrawLine(point, point + spline.GetDirection(i / (float)steps) * directionScale);
            }
        }

        private Vector3 ShowPoint(int index, bool isAnchor = false)
        {
            Vector3 point = spline.handleTransform.TransformPoint(spline.GetControlPoint(index));
            float size = HandleUtility.GetHandleSize(point);
            if (isAnchor)
            {
                size *= 1.5f;
            }
            Handles.color = isAnchor ? Color.yellow : modeColors[(int)spline.GetControlPointMode(index)];
            if (Handles.Button(point, spline.handleRotation, size * handleSize, size * handleSize, Handles.DotHandleCap))
            {
                spline.selectedIndex = index;
                spline.selectedAnchor = Mathf.RoundToInt(index / 3f) * 3;
                Repaint();
            }
            if (spline.selectedIndex == index)
            {
                EditorGUI.BeginChangeCheck();
                point = Handles.DoPositionHandle(point, spline.handleRotation);
                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(spline);
                    spline.ApplyDecoratorTranslation();
                    spline.ApplyDecoratorRotation();
                    spline.SetControlPoint(index, spline.handleTransform.InverseTransformPoint(point));
                }
            }
            return point;
        }
    }
}
