﻿using UnityEngine;
using UnityEditor;
using System;

namespace SayiTools
{
    public class EditorGUIHelper
    {
        public const string PROGRESS_TITLE = "Sayi Tools";

        private static GUIStyle FoldoutStyle = null;
        private static GUIStyle FoldoutHeaderStyle = null;

        public static void HeaderLevel1(string headerText)
        {
            Header(headerText, 18, Color.cyan);
        }

        public static void HeaderLevel2(string headerText)
        {
            Header(headerText, 14, Color.cyan);
        }

        public static void Separator()
        {
            GUILayout.Space(5);
            Rect rect = EditorGUILayout.GetControlRect(GUILayout.Height(2));
            rect.height = 2;
            EditorGUI.DrawRect(rect, new Color(0, 0, 0, 0.15f));
            GUILayout.Space(5);
        }

        private static void Header(string headerText, int fontSize, Color colour)
        {
            GUIStyle headerStyle = new GUIStyle();
            headerStyle.fontStyle = FontStyle.Bold;
            headerStyle.fontSize = fontSize;
            headerStyle.normal.textColor = colour;
            headerStyle.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label(headerText, headerStyle);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            GUILayout.Space(EditorGUIUtility.singleLineHeight);
        }

        public static void DrawFoldout(string title, string toggleKey, Action contentFunc, UnityEngine.Object obj, bool isHeader = false)
        {
            DrawFoldout(title, "", toggleKey, contentFunc, obj, isHeader);
        }

        public static void DrawFoldout(string title, string infoMessage, string toggleKey, Action contentFunc, UnityEngine.Object obj, bool isHeader = false)
        {
            toggleKey = MakePrefName("Foldout." + toggleKey, obj);

            bool prevState = EditorPrefs.GetBool(toggleKey, false);
            bool state = EditorGUILayout.Foldout(prevState, title, GetFoldoutStyle(isHeader));

            if (prevState != state)
            {
                EditorPrefs.SetBool(toggleKey, state);
            }

            if (state)
            {
                if (!String.IsNullOrWhiteSpace(infoMessage))
                {
                    EditorGUILayout.HelpBox(infoMessage, MessageType.Info);
                }

                BeginBox();
                contentFunc();
                EndBox();
            }
        }

        public static GUIStyle GetFoldoutStyle(bool isHeader = false)
        {
            if (isHeader)
            {
                if (FoldoutHeaderStyle == null)
                {
                    FoldoutHeaderStyle = new GUIStyle(EditorStyles.foldout);
                    FoldoutHeaderStyle.fontStyle = FontStyle.Bold;
                    FoldoutHeaderStyle.fontSize = 14;
                    FoldoutHeaderStyle.normal.textColor = Color.cyan;
                    FoldoutHeaderStyle.onNormal.textColor = Color.cyan;
                }

                return FoldoutHeaderStyle;
            }
            else
            {
                if(FoldoutStyle == null)
                {
                    FoldoutStyle = new GUIStyle(EditorStyles.foldout);
                }

                return FoldoutStyle;
            }
        }

        public static void BeginBox()
        {
            // the multiple vertical/horizontal layouts and
            // the space after the horizontal is important for proper spacing
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal(GUI.skin.box);
            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            EditorGUILayout.BeginVertical();
        }

        public static void EndBox()
        {
            EditorGUILayout.EndVertical();
            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
            GUILayout.Space(EditorGUIUtility.singleLineHeight);
        }

        public static string GetProgressBarTitle(string title = null)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                return PROGRESS_TITLE;
            }
            else
            {
                return PROGRESS_TITLE + " - " + title;
            }
        }

        public static void FlexSpaceText(string label, string text)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(label);
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField(text);
            EditorGUILayout.EndHorizontal();
        }

        private static string MakePrefName(string name, UnityEngine.Object obj)
        {
            // make sure pref names are unique for objects
            return obj.GetType().Name + "." + obj.name + "." + name;
        }
    }
}