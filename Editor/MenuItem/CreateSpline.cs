﻿using UnityEditor;
using UnityEngine;

namespace SayiTools
{
    public class CreateSpline
    {
        [MenuItem("GameObject/Create Other/Spline")]
        static void CreateSplineObject()
        {
            var spline = new GameObject("Spline");
            var parent = Selection.activeGameObject;
            if (parent)
            {
                spline.transform.parent = parent.transform;
            }
            spline.transform.localPosition = Vector3.zero;
            spline.transform.localRotation = Quaternion.identity;
            spline.transform.localScale = Vector3.one;

            spline.AddComponent<Spline>();
            Selection.activeGameObject = spline;
        }
    }
}