# Shaders
[TOC]

You can find a higher quality preview of all shaders and effects [here](https://imgur.com/gallery/mAZdltr).

# SayiToon
## Fallback Settings
Fallback settings will be used for others when they don't have your Avatars Shaders enabled. This way you can still look good albeit without any specific effects.

To get a better understanding of what your avatar will look like with the fallback settings have a look at the Unity Standard Shader.

## General Rendering Settings
The shader has settings for:
* Changing the Culling Mode
* Textures with Transparency
* Whether environmental light affects the shader
* A Base Brightness Multiplier
* NormalMap

A normal map only affects how lighting hitting the object gets processed and does not change geomtry. That means it will only have an effect on reflective areas, specular highlights or if directional shadows are enabled.

## Base Texture
Instead of a normal texture this Shader uses a Texture Array. This Repository includes tools to create those with ease. One simple way is to right click on any texture and select `SayiTools > Create Texture Array From Selection`. This will create a TextureArray object next to the texture which can be used on the shader.

<img src="https://imgur.com/Sjy7KGV.gif" height="320">
<br></br>

## Shadows
Shadows can be used optionally, but will only work if the material is set up to be affected by environmental light.

You can use Strength and Smoothness options to control how the shadows appear. Alternatively you can provide a gray scale (black and white) texture as a shadow ramp to have better control of how the shadow transition looks. An example for such a shadow ramp can be found [here](../Examples/Textures/shadow-ramp.png).

<img src="https://imgur.com/z1h9sGh.gif" height="320">
<br></br>

## Material and Special Feature Masks
This shader uses the colour channels on the Material and the Feature Mask Textures to map the different effects on the Object.

The simplest way to set these up is to create a grayscale texture for each effect and using the Texture Combiner (Tools > Sayi > ImageCombiner) to generate the final Mask.

### Material Mask Information
* R - reflection/smoothness
* G - specular highlights
* B - world position texture

### Special Feature Mask Information
* R - Hue/Saturation/Value Changes
* G - Wireframe
* B - Rainbow Effect
* A - Colour Inversion

## Material Features
### Reflection / Smoothness
The Smoothness and Reflection sliders will determine their respective property on the areas of the Material that have been mapped to be affected by these settings in the Material Mask.

<img src="https://imgur.com/8lI9J28.gif" height="320">
<br></br>

### Specular Highlight
The Specular Highlight Exponent slider changes how strong the light being reflected off of the masked areas will be.

<img src="https://imgur.com/iVxbMlQ.gif" height="320">
<br></br>

### Reflection on Transparency
A simple flag to enable reflections to also appear on areas of the Material that are supposed to be transparent. An example use case would be glasses being slightly reflective but otherwise being mostly transparent.

### World Position Texture
This sets up a texture that is projected onto the Material as if it is fixed into the world space. When the Mesh moves the texture stays in place.

<img src="https://imgur.com/3JQMSmk.gif" height="320">
<br></br>

## Special Effects
### HSV Settings
Three sliders to change how the colour of the masked areas is being rendered for:
* Hue - the actual colour
* Saturation - how "colourful" it is
* Value - the intensity or brightness of the colour

<img src="https://imgur.com/ioV8tiE.gif" height="320">
<br></br>

### Outline
The outline effect, if enabled, will create an outline around the Mesh. You can specify an HDR colour and modify the width of the outline.

<img src="https://imgur.com/CIVTRgp.gif" height="320">
<br></br>

### Wireframe
If enabled will show a wireframe (triangles) on your mesh. Their colour can be set to an HDR colour and will blend between that colour and a colour automatically determined by the triangles orientation depending on the HDR colours alpha value.

The maximum width of the wireframe is adjustable. The wireframes width will scale with distance, meaning when someone gets closer the lines smoothly turn thinner as to not cover up the whole texture.

A Fade Out Distance can be set at which the wireframe fades out. This is done to counteract the strong aliasing that can occur on the thin lines when looked at from a distance. If you don't like it just set the Distance to a high value.

<img src="https://imgur.com/gnUzLrS.gif" height="320">
<br></br>

### Rainbow Effect
Creates a funny Rainbow Effect that changes over time. Two sliders allow to control the size of the waves and the speed at which it changes.

<img src="https://imgur.com/HBuBN9n.gif" height="320">
<br></br>

### Colour Inversion
Inverts the base colour and applies all other effects afterwards.

<img src="https://imgur.com/Z3uwrOR.gif" height="320">
<br></br>

### Glow Effect
Enables a glow Effect. Supply a Texture with a transparent background and only add colour to the area that is supposed to glow up. There are settings to change the Intensity and enable an automatic colour transition over time.

<img src="https://i.imgur.com/5ChRMxD.gif" height="320">
<br></br>

# Pixelation
The Pixelation shader does exactly as the name implies: pixelating stuff.

To change how drastic the pixelation is you can change the resolution by setting the pixel size in unity units per axis.

<img src="https://imgur.com/9yi3OqV.gif" height="320">
<br></br>

# Three Colour Gradient
A simple shader that blends between three different colours. It is unlit meaning it doens't get affected by lighting, which makes it good for being used as material for an avatars pen.

It uses HDR Colours so you can make it glow in worlds with Bloom enabled in the PostProcessing and you have a few convenient sliders to control how the gradient transitions from one colour to another.

<img src="https://imgur.com/iA88klT.gif" height="320">
<br></br>

# Screen Position Texture
A shader that sets up a texture to only be rendered in screen position coordinates, meaning it will always look the same regardless of where an object moves.
WARNING: Does not work properly in VR!

<img src="https://imgur.com/98r7RLq.gif" height="320">
