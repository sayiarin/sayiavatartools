Shader "Sayiarin/ThreeColourGradient"
{
	Properties
	{
		[HDR]_FirstColour("1st Colour", Color) = (0, 0, 0, 0)
		[HDR]_SecondColour("2nd Colour", Color) = (0, 0, 0, 0)
		[HDR]_ThirdColour("3rd Colour", Color) = (0, 0, 0, 0)
		_CenterPosition("Middle Colour Position", Range(0, 1)) = 0.5
		[Space]
		_FirstColourStart("1st Colour Start Position", Range(0, 1)) = 0
		_FirstColourEnd("1st Colour End Position", Range(0, 1)) = 1
		[Space]
		_SecondColourStart("2nd Colour Start Position", Range(0, 1)) = 0
		_SecondColourEnd("2nd Colour End Position", Range(0, 1)) = 1
		[Space]
		[Enum(Off, 0, Front, 1, Back, 2)] _CullMode("Culling Mode", int) = 2
	}

	SubShader
	{
		Tags 
		{
			"RenderType" = "Opaque"
			"IsEmissive" = "true"
		}

		Cull [_CullMode]

		Pass 
		{
			CGPROGRAM
			#pragma vertex VertexFunction
			#pragma fragment FragmentFunction

			#include "UnityCG.cginc"

			uniform float4 _FirstColour;
			uniform float4 _SecondColour;
			uniform float4 _ThirdColour;
			uniform float _CenterPosition;
			uniform float _FirstColourStart; 
			uniform float _FirstColourEnd;
			uniform float _SecondColourStart;
			uniform float _SecondColourEnd;

			struct MeshData
			{
				float4 vertex: POSITION;
				float2 uv: TEXCOORD0;
			};

			struct Interpolators
			{
				float4 pos: SV_POSITION;
				float2 uv: TEXCOORD0;
			};

			Interpolators VertexFunction(MeshData meshData)
			{
				Interpolators output;
				output.pos = UnityObjectToClipPos(meshData.vertex);
				output.uv = meshData.uv;
				return output;
			}

			float InverseLerp(float a, float b, float value)
			{
				return (value - a) / (b - a);
			}

			fixed4 FragmentFunction(Interpolators fragIn) : SV_TARGET
			{
				if(fragIn.uv.x < _CenterPosition)
				{
					float colourTransitionWeight = saturate(InverseLerp(_FirstColourStart, _FirstColourEnd, fragIn.uv.x / _CenterPosition));
					return lerp(_FirstColour, _SecondColour, colourTransitionWeight);
				}
				else
				{
					float colourTransitionWeight = saturate(InverseLerp(_SecondColourStart, _SecondColourEnd, (fragIn.uv.x - _CenterPosition) / _CenterPosition));
					return lerp(_SecondColour, _ThirdColour, colourTransitionWeight);
				}
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
} 
