﻿Shader "Sayiarin/UnlitColour"
{
    Properties
    {
        [HDR]_Colour("Colour", Color) = (1, 1, 1, 1)
        _MainTex("Texture", 2D) = "white" {}
        [Enum(Off, 0, Front, 1, Back, 2)] _CullMode("Culling Mode", int) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        Cull[_CullMode]

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed4 _Colour;
            UNITY_DECLARE_TEX2D(_MainTex);
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return UNITY_SAMPLE_TEX2D(_MainTex, i.uv) * _Colour;
            }
            ENDCG
        }
    }
}
