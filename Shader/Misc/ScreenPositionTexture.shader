﻿Shader "Sayiarin/ScreenPositionTexture"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [Enum(Off, 0, Front, 1, Back, 2)] _CullMode("Culling Mode", int) = 1
    }
    SubShader
    {
        Pass
        {
            Cull[_CullMode]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 screenPos : TEXCOORD0;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.screenPos = float3(o.vertex.xy, -o.vertex.w);
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = (i.screenPos.xy / i.screenPos.z);
                return tex2D(_MainTex, uv);
            }
            ENDCG
        }
    }
}
