﻿Shader "Sayiarin/Pixelation"
{
	Properties
	{
		_PixelSizeX("Pixel Size X Axis", Range(0, 0.2)) = 0.05
		_PixelSizeY("Pixel Size Y Axis", Range(0, 0.2)) = 0.05
		[Toggle]_AffectedByDistance("Pixel Size Affected By Distance", int) = 1
	}
	CustomEditor "SayiPixelationShaderEditor"

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
			"Queue" = "Transparent"
		}

		LOD 100
		Cull Off

		GrabPass { "_GrabbedTexture"}

		Pass
		{
			CGPROGRAM
			#pragma vertex VertexFunction
			#pragma fragment FragmentFunction

			#include "UnityCG.cginc"

			float _PixelSizeX;
			float _PixelSizeY;
			int _AffectedByDistance;
			sampler2D _GrabbedTexture;

			struct MeshData
			{
				float4 vertex: POSITION;
			};

			struct Interpolator
			{
				float4 vertex: SV_POSITION;
				float4 grabUV: TEXCOORD0;
				float4 objectOrigin: TEXCOORD1;
			};

			Interpolator VertexFunction(MeshData meshData)
			{
				Interpolator output;
				output.vertex = UnityObjectToClipPos(meshData.vertex);
				// grabby grab
				output.grabUV = ComputeGrabScreenPos(output.vertex);
				output.objectOrigin = mul(unity_ObjectToWorld, float4(0.0f, 0.0f, 0.0f, 0.0f));
				return output;
			}

			half4 FragmentFunction(Interpolator fragIn) : COLOR
			{
				float2 distortionValues = float2(_PixelSizeX, _PixelSizeY);

				// apply scaling over distance
				float distanceToCamera = length(fragIn.objectOrigin - _WorldSpaceCameraPos);
				distortionValues /= (1 + _AffectedByDistance * pow(distanceToCamera, 2));

				float2 pixelation = fragIn.grabUV.xy / fragIn.grabUV.w;
				pixelation /= distortionValues;
				pixelation = round(pixelation);
				pixelation *= distortionValues;
				return tex2D(_GrabbedTexture, pixelation);
			}
			ENDCG
		}
	}
}