// (almost) all properties being used by the SayiToon shaders
// some properties may not appear here because it just makes more sense to have them 
// in their own cginc file where they are being used. A good example would be the
// Outline.cginc and it's respective properties since I basically put everything related
// to that into one include file

// toggle wireframe, also affects geometry function
#ifdef _USES_GEOMETRY
uniform int _EnableWireframe;
#endif

// texture variables
#if SAYI_SIMPLE
uniform UNITY_DECLARE_TEX2D(_MainTex);
uniform float4 _MainTex_ST;
#else
uniform UNITY_DECLARE_TEX2DARRAY(_BaseTextures);
uniform float4 _BaseTextures_ST;
uniform float _BaseTextureIndex;
uniform int _LastBaseTexIdx;
#endif

#ifdef SAYI_CUTOUT
uniform float _AlphaCutoff;
#endif

uniform half _Contrast;
uniform half4 _BaseTexTint;
uniform int _ApplyFog;

// normal map
uniform UNITY_DECLARE_TEX2D(_NormalMap);
uniform float4 _NormalMap_ST;
uniform half _NormalMapStrength;

// Material Features, Mask uses rgb channels for different settings
// r - reflection/smoothness
// g - specular highlights
// b - world position texture
#if SAYI_SIMPLE
uniform UNITY_DECLARE_TEX2D(_MaterialFeatureMask);
uniform float4 _MaterialFeatureMask_ST;
#else
uniform UNITY_DECLARE_TEX2DARRAY(_MaterialFeatureMaskArray);
uniform float4 _MaterialFeatureMaskArray_ST;
uniform float _MaterialFeatureMaskIndex;
uniform int _LastMaterialTexIdx;
#endif

uniform half _Reflectiveness;
uniform int _ReflectionIgnoresAlpha;
uniform half _Smoothness;
uniform half _SpecularHighlightExponent;

// matcap stuff
#if SAYI_MATCAP
uniform UNITY_DECLARE_TEX2D(_MatCapTexture);
uniform float4 _MatCapTexture_ST;
uniform half3 _MatCapColourBias;
uniform half _MatCapReflectionOverride;
uniform half _MatCapLightingOverride;
#endif

// screen position texture
uniform int _EnableWorldPosTexture;
uniform sampler2D _WorldPosTexture;
uniform float _WorldPosTextureZoom;

// special feature map, using rgb channels for different settings
// r - HSV Changes
// g - Wireframe
// b - Rainbow Effect
// a - Colour Inversion
#if SAYI_SIMPLE
uniform UNITY_DECLARE_TEX2D(_SpecialFeatureMask);
uniform float4 _SpecialFeatureMask_ST;
#else
uniform UNITY_DECLARE_TEX2DARRAY(_SpecialFeatureMaskArray);
uniform float4 _SpecialFeatureMaskArray_ST;
uniform float _SpecialFeatureMaskIndex;
uniform int _LastSpecialTexIdx;
#endif

// hsv
uniform half _HueShift;
uniform half _SaturationValue;
uniform half _ColourValue;
uniform half4 _HSVTexTint;

// Rainbow Effect
uniform int _EnableRainbowEffect;
uniform half _RainbowSpeed;
uniform half _RainbowWaveSize;

// invert colours
uniform int _InvertColours;

// lighting variables
#if SAYI_LIT
uniform int _EnableDirectionalShadow;
uniform half _ShadowSmoothness;
uniform half _ShadowStrength;
uniform int _EnableShadowRamp;
uniform sampler2D _ShadowRamp;

// fake lighting and shadows, only on unlit version
#else
uniform int _EnableFakeShadows;
uniform half _FakeShadowStrength;
uniform float4 _FakeLightDirection;
uniform sampler2D _FakeShadowRamp;
#endif

// glow
uniform int _EnableGlow;
uniform int _EnableGlowColourChange;
uniform UNITY_DECLARE_TEX2D(_GlowTexture);
uniform float4 _GlowTexture_ST;
uniform half _GlowIntensity;
uniform half _GlowSpeed;

// wireframe
uniform float4 _WireframeColour;
uniform half _WireframeWidth;
uniform half _WireframeFadeOutDistance;

#if SAYI_TRANSPARENT
uniform int _MainColourAlphaAffectsWireframe;
#endif
