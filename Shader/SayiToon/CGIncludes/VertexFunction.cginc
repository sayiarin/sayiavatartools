#include "DataStructures.cginc"

Interpolators VertexFunction(MeshData meshData)
{
	Interpolators output;

	UNITY_SETUP_INSTANCE_ID(meshData);
	UNITY_INITIALIZE_OUTPUT(Interpolators, output);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

	output.pos = UnityObjectToClipPos(meshData.vertex);
	#if SAYI_SIMPLE
	output.baseUV = TRANSFORM_TEX(meshData.uv, _MainTex);
	output.normalUV = TRANSFORM_TEX(meshData.uv, _NormalMap);
	output.specialUV = TRANSFORM_TEX(meshData.uv, _SpecialFeatureMask);
	output.materialUV = TRANSFORM_TEX(meshData.uv, _MaterialFeatureMask);
	output.glowUV = TRANSFORM_TEX(meshData.uv, _GlowTexture);
	#else
	output.baseUV = TRANSFORM_TEX(meshData.uv, _BaseTextures);
	output.specialUV = TRANSFORM_TEX(meshData.uv, _SpecialFeatureMaskArray);
	output.materialUV = TRANSFORM_TEX(meshData.uv, _MaterialFeatureMaskArray);
	output.normalUV = TRANSFORM_TEX(meshData.uv, _NormalMap);
	output.glowUV = TRANSFORM_TEX(meshData.uv, _GlowTexture);
	#endif

	output.worldPosition = mul(unity_ObjectToWorld, meshData.vertex);
	output.viewDirection = normalize(_WorldSpaceCameraPos.xyz - output.worldPosition);

	// convert normals to world normals because that's where light comes from
	// and we want to use them for shadows
	output.worldNormal = UnityObjectToWorldNormal(meshData.normal);

	output.tangent = meshData.tangent;

	#ifdef _NEEDS_LIGHTING_DATA
		TRANSFER_VERTEX_TO_FRAGMENT(output);
 	#endif

	#ifdef _NEEDS_VERTEX_NORMAL
		output.vertexNormal = abs(meshData.normal);
    #endif

	#ifdef _USES_GEOMETRY
		output.edgeDistance = float3(0,0,0);
	#endif
	
	// for matcap
	#if SAYI_MATCAP
		output.matcapUV = mul((float3x3)UNITY_MATRIX_V, output.worldNormal).xy * 0.5 + 0.5;
	#endif

	UNITY_TRANSFER_FOG(output, output.pos);
	return output;
}