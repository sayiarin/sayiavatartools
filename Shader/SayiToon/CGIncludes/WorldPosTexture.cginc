half3 GetAlignedWorldPosTexture(Interpolators fragIn)
{
    fixed3 col_front = tex2D(_WorldPosTexture, fragIn.worldPosition.yx * _WorldPosTextureZoom);
    fixed3 col_side = tex2D(_WorldPosTexture, fragIn.worldPosition.yz * _WorldPosTextureZoom);
    fixed3 col_top = tex2D(_WorldPosTexture, fragIn.worldPosition.zx * _WorldPosTextureZoom);
    
    fixed3 weights = abs(fragIn.worldNormal);
    weights = pow(weights, 16);
    weights /= (weights.x + weights.y + weights.z);

    col_front *= weights.z;
    col_side *= weights.x;
    col_top *= weights.y;

    return col_front + col_side + col_top;
}