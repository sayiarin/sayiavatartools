#ifndef TEX2DARRAY_BLEND_VARIABLES_SETUP
    int texIdxOne;
    int texIdxTwo;
    float texBlendWeight;
    float4 texOne;
    float4 texTwo;
#define TEX2DARRAY_BLEND_VARIABLES_SETUP
#endif

// now with cursed functionality to loop around from last to first idx :>
// only properly works together with a shader gui that sets the lastIdx properly
#define BLEND_TEX2DARRAY_LOOP(tex2Darray, index, target, uv, lastIdx) \
    texIdxOne = index >= lastIdx + 1 ? 0 : floor(index); \
    texIdxTwo = index > lastIdx ? 0 : ceil(index); \
    texBlendWeight = index - float(texIdxOne); \
    texOne = UNITY_SAMPLE_TEX2DARRAY(tex2Darray, float3(uv, texIdxOne)); \
    texTwo = UNITY_SAMPLE_TEX2DARRAY(tex2Darray, float3(uv, texIdxTwo)); \
    target = lerp(texOne, texTwo, texBlendWeight);