static const float PI = 3.14159265f;

// http://beesbuzz.biz/code/16-hsv-color-transforms
half3 ApplyHSVChangesToRGB(float3 colour, float3 hsv)
{
    float vsu = hsv.y * hsv.z * cos(hsv.x * PI / 180);
    float vsw = hsv.y * hsv.z * sin(hsv.x * PI / 180);

    half3 ret;
    ret.r = (.299*hsv.z + .701*vsu + .168*vsw)*colour.r
        +   (.587*hsv.z - .587*vsu + .330*vsw)*colour.g
        +   (.114*hsv.z - .114*vsu - .497*vsw)*colour.b;
    ret.g = (.299*hsv.z - .299*vsu - .328*vsw)*colour.r
        +   (.587*hsv.z + .413*vsu + .035*vsw)*colour.g
        +   (.114*hsv.z - .114*vsu + .292*vsw)*colour.b;
    ret.b = (.299*hsv.z - .300*vsu + 1.25*vsw)*colour.r
        +   (.587*hsv.z - .588*vsu - 1.05*vsw)*colour.g
        +   (.114*hsv.z + .886*vsu - .203*vsw)*colour.b;
    return ret;
}

float3 ApplyContrast(float3 colour, float contrast)
{
    return (colour.rgb - 0.5) * contrast + 0.5;
}
