// everything regarding outline
// probably strongly against any best practices to stuff what's basically a 
// full CGPROGRAM  into a cginc file but I want my main shader file to be 
// less cluttered and who's going to stop me, huh? :>
        
#include "UnityCG.cginc"
        
uniform float _EnableOutline;
uniform float4 _OutlineColour;
uniform float _OutlineWidth;
uniform int _OutlineFoggy;

struct MeshData
{
    float4 vertex: POSITION;
    float3 normal: NORMAL;
};
        
struct VertexData
{
    float4 vertex: SV_POSITION;
    float3 normal: NORMAL;
    UNITY_FOG_COORDS(1)
};

VertexData Vertex(MeshData meshData)
{
    VertexData output;
    meshData.vertex.xyz += meshData.normal * _OutlineWidth;
    output.vertex = UnityObjectToClipPos(meshData.vertex);
    output.normal = meshData.normal;
    UNITY_TRANSFER_FOG(output, output.vertex);
    return output;
}
        
half4 Fragment(VertexData vertexData) : SV_TARGET
{
    half4 colour = _OutlineColour * _EnableOutline;
    half4 foggyColour = colour;
    UNITY_APPLY_FOG(vertexData.fogCoord, foggyColour);
    return lerp(colour, foggyColour, _OutlineFoggy);
}
