#include "ColourUtilities.cginc"
#include "Wireframe.cginc"
#include "Reflection.cginc"
#include "LightUtilities.cginc"
#include "WorldPosTexture.cginc"
#include "TextureUtilities.cginc"

half4 FragmentFunction (Interpolators fragIn) : SV_TARGET
{
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(fragIn);

    // normal map magic
    float4 worldTangent = float4(UnityObjectToWorldDir(fragIn.tangent.xyz), fragIn.tangent.w);
    float3 binormalTangent = cross(fragIn.worldNormal, worldTangent.xyz) * (worldTangent.w * unity_WorldTransformParams.w);
    float4 normal = lerp(float4(0.5, 0.5, 1, 1), UNITY_SAMPLE_TEX2D(_NormalMap, fragIn.normalUV), _NormalMapStrength);
    float3 tangentSpaceNormal = UnpackNormal(normal);
    fragIn.worldNormal = normalize(
        tangentSpaceNormal.x * worldTangent.xyz +
        tangentSpaceNormal.y * binormalTangent +
        tangentSpaceNormal.z * fragIn.worldNormal
    );

    half4 colour;
    #if SAYI_SIMPLE
    colour = UNITY_SAMPLE_TEX2D(_MainTex, fragIn.baseUV);
    #else
    BLEND_TEX2DARRAY_LOOP(_BaseTextures, _BaseTextureIndex, colour, fragIn.baseUV, _LastBaseTexIdx);
    #endif

    colour *= _BaseTexTint;

    #if SAYI_CUTOUT
        clip(colour.a - _AlphaCutoff);
    #endif

    // matcap
    #if SAYI_MATCAP
        half3 matCapTex = UNITY_SAMPLE_TEX2D(_MatCapTexture, fragIn.matcapUV);
        matCapTex *= _MatCapColourBias;
    #endif

    // special effects
    half4 specialEffectsFeatureMask;
    #if SAYI_SIMPLE
    specialEffectsFeatureMask = UNITY_SAMPLE_TEX2D(_SpecialFeatureMask, fragIn.specialUV);
    #else
    BLEND_TEX2DARRAY_LOOP(_SpecialFeatureMaskArray, _SpecialFeatureMaskIndex, specialEffectsFeatureMask, fragIn.specialUV, _LastSpecialTexIdx);
    #endif

    // colour inversion is just 1 - colour
    colour.rgb = lerp(colour.rgb, 1 - colour.rgb, _InvertColours * specialEffectsFeatureMask.a);

    // hsv stuff, using red channel of feature mask
    half3 rgbNew = _HSVTexTint;
    rgbNew *= ApplyHSVChangesToRGB(colour.rgb, float3(_HueShift, _SaturationValue, _ColourValue));
    colour.rgb = lerp(colour.rgb, rgbNew, specialEffectsFeatureMask.r);

    // apply contrast
    colour.rgb = ApplyContrast(colour.rgb, _Contrast);

    #ifdef UNITY_PASS_FORWARDBASE
        half3 materialFeatureMask;
        #if SAYI_SIMPLE
        materialFeatureMask = UNITY_SAMPLE_TEX2D(_MaterialFeatureMask, fragIn.materialUV);
        #else
        BLEND_TEX2DARRAY_LOOP(_MaterialFeatureMaskArray, _MaterialFeatureMaskIndex, materialFeatureMask, fragIn.materialUV, _LastMaterialTexIdx);
        #endif

        // specular highlights, using blinn phong
        half3 specularLight = SpecularLight(fragIn.worldNormal, fragIn.viewDirection, colour);
        colour.rgb = lerp(colour.rgb, specularLight, materialFeatureMask.g);

        // reflection using red channel
        half3 reflectionValue = GetReflection(fragIn);
        #if SAYI_MATCAP
            reflectionValue = lerp(reflectionValue, matCapTex, _MatCapReflectionOverride);
        #endif
        reflectionValue = lerp(colour.rgb, reflectionValue, _Reflectiveness);

        colour.rgb = lerp(colour.rgb, reflectionValue, materialFeatureMask.r);
        // lerp between original and full reflectiveness depending on whether the setting is activated,
        // the colour is actually of any value (reflection exists) and the area has reflection
        colour.a = lerp(colour.a, 1.0, _ReflectionIgnoresAlpha * colour.rgb * materialFeatureMask.r);
    #endif

    // apply lighting after reflections had been calculated
    #if SAYI_MATCAP
        half3 lightColour = CorrectedLightColour(fragIn);
        colour.rgb *= lerp(lightColour, lightColour * matCapTex, _MatCapLightingOverride);
    #else
        colour.rgb *= CorrectedLightColour(fragIn);
    #endif

    // these effects are supposed to never be affected by shadows/light so they get calculated after light colour
    #ifdef UNITY_PASS_FORWARDBASE
        // apply world space aligned 2d texture
        half3 alignedWorldPosTexture = GetAlignedWorldPosTexture(fragIn);
        colour.rgb = lerp(colour.rgb, alignedWorldPosTexture, materialFeatureMask.b * _EnableWorldPosTexture);

        // glowwy
        half4 glowColour = UNITY_SAMPLE_TEX2D(_GlowTexture, fragIn.glowUV);
        half glowHueShift = lerp(0, (_Time.y / _GlowSpeed), _EnableGlowColourChange);
        glowColour.rgb = ApplyHSVChangesToRGB(glowColour.rgb, half3(glowHueShift, 1, _GlowIntensity));
        colour = lerp(colour, glowColour, glowColour.a * _EnableGlow);

        // rainbow colour effect
        // for now just a colour change over time based on normal direction, nothing fancy
        // but it looks neat
        half3 rainbowColour = half3(sin(normalize(fragIn.worldNormal).xyz * _RainbowWaveSize)) * 0.5 + 0.5;
        rainbowColour = ApplyHSVChangesToRGB(rainbowColour, half3(_Time.y / _RainbowSpeed * 360, 1, 1));
        colour.rgb = lerp(colour.rgb, rainbowColour.rgb, specialEffectsFeatureMask.b * _EnableRainbowEffect);

        // wireframe
        #if SAYI_TRANSPARENT
            half4 wireframeColour = ApplyWireframeColour(colour, fragIn, specialEffectsFeatureMask.g, _EnableWireframe);
            colour = lerp(wireframeColour, wireframeColour * colour.a, _MainColourAlphaAffectsWireframe);
        #else
            colour = ApplyWireframeColour(colour, fragIn, specialEffectsFeatureMask.g, _EnableWireframe);
        #endif
    #endif

    float4 fogColour = colour;
    UNITY_APPLY_FOG(fragIn.fogCoord, fogColour);
    colour = lerp(colour, fogColour, _ApplyFog);
    return colour;
}