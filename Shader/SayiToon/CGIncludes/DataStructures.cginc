// very basic data structures for simple shaders that don't need anything special
#ifndef __DATA_STRUCTURES__
#define __DATA_STRUCTURES__

// per vertex mesh data to be fed into the Vertex function
// also appdata is a terrible name
struct MeshData
{
    float4 vertex: POSITION;
    float2 uv: TEXCOORD0;
    float3 normal: NORMAL0;
    float4 tangent: TANGENT;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

// Interpolators getting passed around
struct Interpolators
{
    // sadly I have to call it pos because that's the only variable name
    // the unity lighting macro seems to accept :<
    float4 pos: SV_POSITION;
    float3 worldNormal: NORMAL0;
    float2 baseUV: TEXCOORD0;
    float2 normalUV: TEXCOORD1;
    float2 specialUV: TEXCOORD2;
    float2 materialUV: TEXCOORD3;
    float2 glowUV: TEXCOORD4;
    float4 worldPosition: TEXCOORD5;
    float3 viewDirection: TEXCOORD6;
    float4 tangent: TEXCOORD7;

    // shader features
    #if SAYI_MATCAP
        float2 matcapUV: TEXCOORD11;
    #endif

    // shader pass specific defines
    #ifdef _NEEDS_LIGHTING_DATA
        UNITY_LIGHTING_COORDS(9, 10)
    #endif

    #ifdef _USES_GEOMETRY
        float3 edgeDistance: TEXCOORD8;
    #endif

    UNITY_FOG_COORDS(12)
    UNITY_VERTEX_OUTPUT_STEREO
};
#endif